
public class Driver {

	public static void main(String[] args) {
		
		BST<Integer> bst = new BST<Integer>();
		
		bst.add(27);
		bst.add(14);
		bst.add(10);
		bst.add(35);
		bst.add(31);
		bst.add(19);
		bst.add(42);
		bst.add(null);
		
		System.out.println(bst.inOrder());
//		System.out.println(bst.preOrder());
//		System.out.println(bst.postOrder());
//		System.out.println();;
//		
//		System.out.println("The following should be true");
//		System.out.println(bst.contains(27));
//		System.out.println(bst.contains(31));
//		System.out.println(bst.contains(14));
//		System.out.println(bst.contains(19));
//		System.out.println(bst.contains(27));
//		System.out.println(bst.contains(35));
//		System.out.println(bst.contains(10));
//		System.out.println();
//		
//		System.out.println("The following should be false");
//		System.out.println(bst.contains(25));
//		System.out.println(bst.contains(1));
//		System.out.println(bst.contains(78));
//		System.out.println(bst.contains(28));
//		System.out.println(bst.contains(33));
//		System.out.println(bst.contains(15));
//		System.out.println(bst.contains(11));
//		System.out.println();
		
		bst.remove(73);
		System.out.println(bst.inOrder());
	}
	
}

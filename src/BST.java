import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class BST<T extends Comparable<T>> {

	private Node<T> root;
	private int size;
	
	public BST() {
		root = null;
		size = 0;
	}
	
	/**
	 * Adds a data entry to the BST
	 * 
	 * null is positive infinity
	 * 
	 * @param data The data entry to add
	 */
	public void add(T data) {
		
		if (root == null) {
			root = new Node<T>(data);
		} else {
			addHelper(root, data);
		}
		
		size++;
	}
	
	private Node<T> addHelper(Node<T> node, T data) {
		if (node == null) {
			return new Node<T>(data);
		} else {
			if (isLessThan(data, node.getData())) {
				Node<T> newNode = addHelper(node.getLeft(), data);
				node.setLeft(newNode);
			} else {
				Node<T> newNode = addHelper(node.getRight(), data);
				node.setRight(newNode);
			}
			
			return node;
		}
	}
	
	private boolean isLessThan(T data1, T data2) {
		if (data1 == null) {
			return false;
		} else if (data2 == null) {
			return true;
		} else {
			return data1.compareTo(data2) < 0;
		}
	}
	
	/**
	 * Adds each data entry from the collection to this BST
	 * 
	 * @param c
	 */
	public void addAll(Collection<? extends T> c) {
		for (T element : c) {
			add(element);
		}
	}
	
	/**
	 * Removes a data entry from the BST
	 * 
	 * null is positive infinity
	 * 
	 * @param data The data entry to be removed
	 * @return The removed data entry (null if nothing is removed)
	 */
	public T remove(T data) {
		T value = null;
		
		if (isEmpty()) {
			return null;
		} else if (size == 1) {
			value = root.getData();
			clear();
		} else {
			value = removeHelper(root, root, data);
		}
		
		return value;
	}
	
	private T removeHelper(Node<T> currentNode, Node<T> prevNode, T data) {
		T valueRemoved = null;
		
		if (currentNode == null) {
			return null;
		} else if (areEqual(currentNode.getData(), data)) {
			valueRemoved = currentNode.getData();
			
			Node<T> predecessor = getPredecessor(currentNode);
			
			if (currentNode.equals(root)) {
				root = predecessor;
			} else if (currentNode.equals(prevNode.getLeft())) {
				prevNode.setLeft(predecessor);
			} else {
				prevNode.setRight(predecessor);
			}
			
			if (predecessor != null) {
				predecessor.setLeft(currentNode.getLeft());
				predecessor.setRight(currentNode.getRight());
			}
			
			size--;
			
		} else {
			if (isLessThan(data, currentNode.getData())) {
				valueRemoved = removeHelper(currentNode.getLeft(), currentNode, data);
			} else {
				valueRemoved = removeHelper(currentNode.getRight(), currentNode, data);
			}
		}
		
		return valueRemoved;
	}
	
	private Node<T> getPredecessor(Node<T> node) {
		if (node.getLeft() == null) {
			Node<T> currentNode = node.getRight();
			
			node.setRight(null);
			
			return currentNode;
		} else {
			Node<T> prevNode = node;
			Node<T> currentNode = node.getLeft();
			
			while(currentNode.getRight() != null) {
				prevNode = currentNode;
				currentNode = currentNode.getRight();
			}
			
			if (currentNode.equals(prevNode.getLeft())) {
				prevNode.setLeft(null);
			} else {
				prevNode.setRight(null);
			}
			
			return currentNode;
		}
	}
	
	/**
	 * Checks if the BST contains a data entry
	 * 
	 * null is positive infinity
	 * 
	 * @param data The data entry to be checked
	 * @return If the data entry is in the BST 
	 */
	public boolean contains(T data) {
		return containsHelper(root, data);
	}
	
	private boolean containsHelper(Node<T> node, T data) {
		if (node == null) {
			return false;
		} else if (areEqual(node.getData(), data)) {
			return true;
		} else {
			boolean result = false;
			if (isLessThan(data, node.getData())) {
				result = containsHelper(node.getLeft(), data);
			} else {
				result = containsHelper(node.getRight(), data);
			}
			
			return result;
		}
	}
	
	private boolean areEqual(T data1, T data2) {
		if (data1 != null) {
			return data1.equals(data2);
		} else {
			return data1 == data2;
		}
	}
	
	/**
	 * Finds the pre-order traversal of the BST
	 * 
	 * @return A list of the data set in the BST in pre-order
	 */
	public List<T> preOrder() {
		List<T> preOrderList = new LinkedList<T>();
		
		if (root != null) {
			preOrderHelper(root, preOrderList);
		}
		
		return preOrderList;
	}
	
	private void preOrderHelper(Node<T> currentNode, List<T> traversalList) {
		if (currentNode == null) {
			return;
		} else {
			traversalList.add(currentNode.getData());
			preOrderHelper(currentNode.getLeft(), traversalList);
			preOrderHelper(currentNode.getRight(), traversalList);
		}
	}

	/**
	 * Finds the in-order traversal of the BST
	 * 
	 * @return A list of the data set in the BST in in-order
	 */
	public List<T> inOrder() {
		List<T> inOrderList = new LinkedList<T>();
		
		if (root != null) {
			inOrderHelper(root, inOrderList);
		}
		
		return inOrderList;
	}
	
	private void inOrderHelper(Node<T> currentNode, List<T> traversalList) {
		if (currentNode == null) {
			return;
		} else {
			inOrderHelper(currentNode.getLeft(), traversalList);
			traversalList.add(currentNode.getData());
			inOrderHelper(currentNode.getRight(), traversalList);
		}
	}
	
	/**
	 * Finds the post-order traversal of the BST
	 * 
	 * @return A list of the data set in the BST in post-order
	 */
	public List<T> postOrder() {
		List<T> postOrderList = new LinkedList<T>();
		
		if (root != null) {
			postOrderHelper(root, postOrderList);
		}
		
		return postOrderList;
	}
	
	private void postOrderHelper(Node<T> currentNode, List<T> traversalList) {
		
		if (currentNode == null) {
			return;
		} else {
			postOrderHelper(currentNode.getLeft(), traversalList);
			postOrderHelper(currentNode.getRight(), traversalList);
			traversalList.add(currentNode.getData());
		}
		
	}
	
	/**
	 * Checks to see if the BST is empty
	 * 
	 * @return If the BST is empty or not
	 */
	public boolean isEmpty() {
		return size == 0;
	}
	
	/**
	 * Clears this BST
	 */
	public void clear() {
		root = null;
		size = 0;
	}
	
	/**
	 * @return the size of this BST
	 */
	public int size() {
		return size;
	}
	
	private class Node<T extends Comparable<T>> {
		
		private T data;
		private Node<T> left;
		private Node<T> right;
		
		public Node(T data) {
			this.data = data;
		}
		
		public T getData() {
			return data;
		}
		
		public Node<T> getLeft() {
			return left;
		}
		
		public Node<T> getRight() {
			return right;
		}
		
		public void setLeft(Node<T> left) {
			this.left = left;
		}
		
		public void setRight(Node<T> right) {
			this.right = right;
		}
	}
	
}
